<?
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();
/**
 * Bitrix vars
 *
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponentTemplate $this
 * @global CMain $APPLICATION
 * @global CUser $USER
 */
?>


<div class="main">
<br>
<?if(!empty($arResult["ERROR_MESSAGE"]))
{
	foreach($arResult["ERROR_MESSAGE"] as $v)
		ShowError($v);
}
if(strlen($arResult["OK_MESSAGE"]) > 0)
{
	?><div class="success_mes"><?=$arResult["OK_MESSAGE"]?></div><?
}
?>
    <h1>Ваш отзыв:</h1>

    <form action="<?=POST_FORM_ACTION_URI?>" method="POST" id = 'demo-form'>
<?=bitrix_sessid_post()?>
	<div>
		<div class="title">
			<?=GetMessage("MFT_NAME")?><?if(empty($arParams["REQUIRED_FIELDS"]) || in_array("NAME", $arParams["REQUIRED_FIELDS"])):?><span class="mf-req">*</span><?endif?>
		</div>
		<input class="text_field" type="text" name="user_name" value="<?=$arResult["AUTHOR_NAME"]?>">
	</div>
    <div>
        <div class="title">
            <?=GetMessage("MFT_LAST")?><?if(empty($arParams["REQUIRED_FIELDS"]) || in_array("LAST", $arParams["REQUIRED_FIELDS"])):?><span class="mf-req">*</span><?endif?>
        </div>
        <input class="text_field" type="text" name="user_last" value="<?=$arResult["AUTHOR_LAST"]?>">
    </div>
	<div>
		<div class="title">
			<?=GetMessage("MFT_EMAIL")?><?if(empty($arParams["REQUIRED_FIELDS"]) || in_array("EMAIL", $arParams["REQUIRED_FIELDS"])):?><span class="mf-req">*</span><?endif?>
		</div>
		<input class="text_field" type="text" name="user_email" value="<?=$arResult["AUTHOR_EMAIL"]?>">
	</div>

    <div>
        <div class="title">
            <?=GetMessage("MFT_PHONE")?><?if(empty($arParams["REQUIRED_FIELDS"]) || in_array("LAST", $arParams["REQUIRED_FIELDS"])):?><span class="mf-req">*</span><?endif?>
        </div>
        <input class="text_field" type="tel" name="user_phone" value="<?=$arResult["AUTHOR_PHONE"]?>">
    </div>

	<div>
		<div class="title">
			<?=GetMessage("MFT_MESSAGE")?><?if(empty($arParams["REQUIRED_FIELDS"]) || in_array("MESSAGE", $arParams["REQUIRED_FIELDS"])):?><span class="mf-req">*</span><?endif?>
		</div>
		<textarea class="textar" name="MESSAGE" rows="5" cols="40"><?=$arResult["MESSAGE"]?></textarea>
	</div>

	<input type="hidden" name="PARAMS_HASH" value="<?=$arResult["PARAMS_HASH"]?>">
    <input type="hidden" name="recaptcha_response" id="recaptchaResponse">
    <input type="submit" class="sbm" name="submit" value="<?=GetMessage("MFT_SUBMIT")?>">
</form>

    <?if (!empty($arResult["DATA"])):?>
    <br>
    <h1>Отзывы:</h1>
    <table>
        <thead>
            <tr>
                <td>Имя</td>
                <td>Фамилия</td>
                <td>E-mail</td>
                <td>Телефон</td>
                <td>Текст отзыва</td>
            </tr>
        </thead>
        <tbody>
            <?foreach ($arResult["DATA"] as $id => $item):?>
                <tr>
                    <?foreach ($item as $key => $val):?>
                        <td><?=$val?></td>
                    <?endforeach?>
                </tr>
            <?endforeach?>
        </tbody>
    </table><br><br><br><br><br>
    <?endif?>
</div>

<?if($arParams["USE_CAPTCHA"] == 'Y'):?>
<script src="https://www.google.com/recaptcha/api.js?onload=onloadCallbackRecap&render=6LcCALoZAAAAAKcwHCMnyizrsU-WB3tJ6sHD7A9a" async defer></script>
<script>
    var onloadCallbackRecap = function() {
        grecaptcha.ready(function () {
            grecaptcha.execute('6LcCALoZAAAAAKcwHCMnyizrsU-WB3tJ6sHD7A9a', { action: 'contact_callback' }).then(function (token) {
                var recaptchaResponse = document.getElementById('recaptchaResponse');
                recaptchaResponse.value = token;
            });
        });
    };
</script>
<?endif?>