<?php
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();

/**
 * Bitrix vars
 *
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponent $this
 * @global CMain $APPLICATION
 * @global CUser $USER
 */



$arResult["PARAMS_HASH"] = md5(serialize($arParams).$this->GetTemplateName());

$arParams["USE_CAPTCHA"] = (($arParams["USE_CAPTCHA"] != "N" && !$USER->IsAuthorized()) ? "Y" : "N");
$arParams["EVENT_NAME"] = trim($arParams["EVENT_NAME"]);
if($arParams["EVENT_NAME"] == '')
	$arParams["EVENT_NAME"] = "FEEDBACK_FORM";
$arParams["EMAIL_TO"] = trim($arParams["EMAIL_TO"]);
if($arParams["EMAIL_TO"] == '')
	$arParams["EMAIL_TO"] = COption::GetOptionString("main", "email_from");
$arParams["OK_TEXT"] = trim($arParams["OK_TEXT"]);
if($arParams["OK_TEXT"] == '')
	$arParams["OK_TEXT"] = GetMessage("MF_OK_MESSAGE");

if($_SERVER["REQUEST_METHOD"] == "POST" && $_POST["submit"] <> '' && (!isset($_POST["PARAMS_HASH"]) || $arResult["PARAMS_HASH"] === $_POST["PARAMS_HASH"]))
{
	$arResult["ERROR_MESSAGE"] = array();
	if(check_bitrix_sessid())
	{

        if(empty($arParams["REQUIRED_FIELDS"]) || !in_array("NONE", $arParams["REQUIRED_FIELDS"]))
		{

			if((empty($arParams["REQUIRED_FIELDS"]) || in_array("NAME", $arParams["REQUIRED_FIELDS"])) && strlen($_POST["user_name"]) <= 1)
				$arResult["ERROR_MESSAGE"][] = GetMessage("MF_REQ_NAME");		
			if((empty($arParams["REQUIRED_FIELDS"]) || in_array("LAST", $arParams["REQUIRED_FIELDS"])) && strlen($_POST["user_last"]) <= 1)
				$arResult["ERROR_MESSAGE"][] = GetMessage("MF_REQ_LAST");
			if((empty($arParams["REQUIRED_FIELDS"]) || in_array("EMAIL", $arParams["REQUIRED_FIELDS"])) && strlen($_POST["user_email"]) <= 1)
				$arResult["ERROR_MESSAGE"][] = GetMessage("MF_REQ_EMAIL");
			if((empty($arParams["REQUIRED_FIELDS"]) || in_array("PHONE", $arParams["REQUIRED_FIELDS"])) && strlen($_POST["user_phone"]) <= 1)
				$arResult["ERROR_MESSAGE"][] = GetMessage("MF_REQ_PHONE");
			if((empty($arParams["REQUIRED_FIELDS"]) || in_array("MESSAGE", $arParams["REQUIRED_FIELDS"])) && strlen($_POST["MESSAGE"]) <= 3)
				$arResult["ERROR_MESSAGE"][] = GetMessage("MF_REQ_MESSAGE");
		}
		if(strlen($_POST["user_email"]) > 1 && !check_email($_POST["user_email"]))
			$arResult["ERROR_MESSAGE"][] = GetMessage("MF_EMAIL_NOT_VALID");

        $clean_phone = preg_replace('/\s|\+|-|\(|\)/','', $_POST["user_phone"]);

        if (strlen($_POST["user_phone"]) > 1){
            if((strlen($clean_phone) == 11 && in_array(mb_substr($clean_phone,0,1), ['7','8'])) == false)
                $arResult["ERROR_MESSAGE"][] = GetMessage("MF_PHONE_NOT_VALID");
        }



		if($arParams["USE_CAPTCHA"] == "Y")
		{
            // Google Captcha
            if (isset($_POST['recaptcha_response'])){

                $recaptcha_key = '6LcCALoZAAAAAOGQwrpGl8TtV63DvnUKVGY03Mcc';
                $recaptcha_url = 'https://www.google.com/recaptcha/api/siteverify';
                $recaptcha_params = [
                    'secret' => $recaptcha_key,
                    'response' => $_POST['recaptcha_response'],
                    'remoteip' => $_SERVER['REMOTE_ADDR'],
                ];

                $ch = curl_init($recaptcha_url);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $recaptcha_params);
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
                curl_setopt($ch, CURLOPT_HEADER, 0);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

                $response = curl_exec($ch);
                if (!empty($response)) {
                    $decoded_response = json_decode($response);
                }

                $recaptcha_success = false;

                if ($decoded_response && $decoded_response->score > 0) {
                    $recaptcha_success = $decoded_response->score;
                    $go = false;

                    if($recaptcha_success > 0.5){
                        $go = true;
                    }

                } else {
                    // Если пользователь оказался ботом
                    $arResult["ERROR_MESSAGE"][] = 'You bot';
                }

            }
            else{
                $arResult["ERROR_MESSAGE"][] = 'Ошибка reCaptcha';
            }

		}
		else{
		    $go = true;
        }


		if(empty($arResult["ERROR_MESSAGE"]) and $go){


            if(CModule::IncludeModule("iblock")){
                $el = new CIBlockElement;;

                $arLoadProductArray = array(
                    "MODIFIED_BY" => $USER->GetID(), // элемент изменен текущим пользователем
                    "IBLOCK_SECTION_ID" => false,
                    "IBLOCK_ID" => 5,
                    "PROPERTY_VALUES" => array(
                        'AUTHOR' => $_POST["user_name"],
                        'LAST' => $_POST["user_last"],
                        'PHONE' => $_POST["user_phone"],
                        'MESSAGE' => $_POST["MESSAGE"]
                    ),
                    "NAME" => $arParams["EMAIL_TO"],
                    "ACTIVE" => "Y",
                );

                $el->Add($arLoadProductArray);
            }

            $arFields = Array(
				"AUTHOR" => $_POST["user_name"],
				"LAST" => $_POST["user_last"],
				"AUTHOR_EMAIL" => $_POST["user_email"],
				"EMAIL_TO" => $arParams["EMAIL_TO"],
				"PHONE" => $_POST["user_phone"],
				"TEXT" => $_POST["MESSAGE"],
			);

			if(!empty($arParams["EVENT_MESSAGE_ID"]))
			{
				foreach($arParams["EVENT_MESSAGE_ID"] as $v)
					if(IntVal($v) > 0)
						CEvent::Send($arParams["EVENT_NAME"], SITE_ID, $arFields, "N", IntVal($v));
			}
			else
				CEvent::Send($arParams["EVENT_NAME"], SITE_ID, $arFields);
			$_SESSION["MF_NAME"] = htmlspecialcharsbx($_POST["user_name"]);
            $_SESSION["MF_LAST"] = htmlspecialcharsbx($_POST["user_last"]);
			$_SESSION["MF_EMAIL"] = htmlspecialcharsbx($_POST["user_email"]);
			$_SESSION["MF_PHONE"] = htmlspecialcharsbx($_POST["user_phone"]);
			$_SESSION["MF_MESSAGE"] = htmlspecialcharsbx($_POST["MESSAGE"]);
			LocalRedirect($APPLICATION->GetCurPageParam("success=".$arResult["PARAMS_HASH"], Array("success")));
		}
		
		$arResult["MESSAGE"] = htmlspecialcharsbx($_POST["MESSAGE"]);
		$arResult["AUTHOR_NAME"] = htmlspecialcharsbx($_POST["user_name"]);
		$arResult["AUTHOR_LAST"] = htmlspecialcharsbx($_POST["user_last"]);
		$arResult["AUTHOR_EMAIL"] = htmlspecialcharsbx($_POST["user_email"]);
		$arResult["AUTHOR_PHONE"] = htmlspecialcharsbx($_POST["user_phone"]);
	}
	else
		$arResult["ERROR_MESSAGE"][] = GetMessage("MF_SESS_EXP");
}
elseif($_REQUEST["success"] == $arResult["PARAMS_HASH"])
{
	$arResult["OK_MESSAGE"] = $arParams["OK_TEXT"];
}

if(empty($arResult["ERROR_MESSAGE"]))
{
	if($USER->IsAuthorized())
	{
		$arResult["AUTHOR_NAME"] = $USER->GetFormattedName(false);
		$arResult["AUTHOR_EMAIL"] = htmlspecialcharsbx($USER->GetEmail());
	}
	else
	{
		if(strlen($_SESSION["MF_NAME"]) > 0)
			$arResult["AUTHOR_NAME"] = htmlspecialcharsbx($_SESSION["MF_NAME"]);
		if(strlen($_SESSION["MF_LAST"]) > 0)
			$arResult["AUTHOR_LAST"] = htmlspecialcharsbx($_SESSION["MF_LAST"]);
		if(strlen($_SESSION["MF_EMAIL"]) > 0)
			$arResult["AUTHOR_EMAIL"] = htmlspecialcharsbx($_SESSION["MF_EMAIL"]);
		/*if(strlen($_SESSION["MF_PHONE"]) > 0)
			$arResult["AUTHOR_PHONE"] = htmlspecialcharsbx($_SESSION["MF_PHONE"]);
		if(strlen($_SESSION["MF_MESSAGE"]) > 0)
			$arResult["MF_MESSAGE"] = htmlspecialcharsbx($_SESSION["MF_MESSAGE"]);*/
	}
}

if($arParams["USE_CAPTCHA"] == "Y")
	$arResult["capCode"] =  htmlspecialcharsbx($APPLICATION->CaptchaGetCode());


if(CModule::IncludeModule("iblock")){


    $arSelect = Array("ID", "ACTIVE", "NAME", "PROPERTY_AUTHOR", "PROPERTY_LAST", "PROPERTY_PHONE", "PROPERTY_MESSAGE");
    $arFilter = Array("IBLOCK_ID"=>5, "ACTIVE"=>"Y");
    $res = CIBlockElement::GetList(Array('ID'=>'DESC'), $arFilter, false, Array(), $arSelect);
    while($ob = $res->GetNextElement())
    {
        $arFields = $ob->GetFields();

        $arResult["DATA"][] = array(
            'AUTHOR' => $arFields["PROPERTY_AUTHOR_VALUE"],
            'LAST' => $arFields["PROPERTY_LAST_VALUE"],
            'MAIL' => $arFields["NAME"],
            'PHONE' => $arFields["PROPERTY_PHONE_VALUE"],
            'MESSAGE' => $arFields["PROPERTY_MESSAGE_VALUE"],
        );
    }
}


$this->IncludeComponentTemplate();
